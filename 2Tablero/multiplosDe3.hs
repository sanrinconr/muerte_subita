--SACADO DE INTERNET https://stackoverflow.com/questions/3963269/split-a-number-into-its-digits-with-haskell
separar :: Integral x => x -> [x]
separar 0 = []
separar x = separar (div x 10) ++ [mod x 10]
-- ***********************************

separarLista::[Int]->[[Int]]
separarLista x = [ separar r | r<-x]

divisores::[Int]->[Int]
divisores [ ]= [ ]
divisores (x:xs) = if (mod x 3) == 0
      then [x] ++ divisores xs
      else divisores xs

filtrarDivisores::[[Int]]->[Int]
filtrarDivisores [] = []
filtrarDivisores (x:sa) = divisores x ++ filtrarDivisores(sa)
